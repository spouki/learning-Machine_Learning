Hello,

This repository contains my work on the understanding of how Perceptrons works.

So I begin by the base, as implementing simple two inputs neurons (AND , OR) as defined [here] (http://computing.dcu.ie/~humphrys/Notes/Neural/single.neural.html).

The diverse classes you will found in this repository are :<br/>

-A_Perceptron (abstract)

-AND_Perceptron (inherits from A_Perceptron)

-OR_Perceptron (inherits from A_Perceptron)

```mermaid
    graph TD;
    A_Perceptron-->AND_Perceptron;
    A_Perceptron-->OR_Perceptron;
```

A_Perceptron defines the base class which is derived for inheritance and also define a compute method for getting the result based on the following equation

$`\sum\nolimits_{i} W_i * I_i`$

Where :

- W are the weights

- I are the inputs

Then when the sum is computed, we compare it to the treshold value and
```javascript
if sum >= treshold
   return 1
else
   return 0
```


The following classes implements their weights and treshold value, as they are different (AND weights != OR weights as AND treshold != OR treshold).

AND_Perceptron is a perceptron that gives us a comparison between two inputs A and B, then evaluate the validness of the two arguments and returns an output 1 or 0.

OR_Perceptron is a perceptron that gives us a comparison between two inputs A and , then evaluate the validness of at least one of the two arguments and returns an output 1 or 0.


The sources I use for learning how basic artificial neurons (perceptrons) works :

* http://computing.dcu.ie/~humphrys/Notes/Neural/single.neural.html
* http://www.inf.fu-berlin.de/inst/ag-ki/rojas_home/documents/1996/NeuralNetworks/K3.pdf
* http://www.inf.fu-berlin.de/inst/ag-ki/rojas_home/documents/1996/NeuralNetworks/K4.pdf
* http://computing.dcu.ie/~humphrys/ca318/index.html#rojas
* http://cs231n.stanford.edu/
* http://neuralnetworksanddeeplearning.com/chap1.html
