#!/usr/bin/env python3.6

from a_perceptron import *

s1 = [0.1,0.2]
w1 = [0.5,0.9]
outps = [1,1]

def main():
    and_p = AND_Perceptron()
    or_p = OR_Perceptron()
    not_p = NOT_Perceptron()
    o = and_p.compute([1,1])
    print(o)
    o1 = or_p.compute([1,0])
    print(o1)
    of = and_p.compute([o,o1])
    print(of)
    no = not_p.compute([1])
    print(no)

if __name__=='__main__':
    main()

# END #
