from abc import ABC

class A_Perceptron(ABC):
    def __init__(self):
        self.weights = []
        self.treshold = None

    def compute(self,values):
        if len(self.weights) != len(values):
            return None
        o = 0
        for idx in range(len(self.weights)):
            o += self.weights[idx] * values[idx]
        if o >= self.treshold:
            return 1
        return 0


class AND_Perceptron(A_Perceptron):
    def __init__(self):
        self.weights = [1,1]
        self.treshold = 2

class OR_Perceptron(A_Perceptron):
    def __init__(self):
        self.weights = [1,1]
        self.treshold = 1

class NOT_Perceptron(A_Perceptron):
    def __init__(self):
        self.weights = [-1]
        self.treshold = 0

# END #
